package code

import java.util.*

class Path {

    var sequence: MutableList<Edge> = LinkedList()
    private var mCost = java.lang.Double.POSITIVE_INFINITY
    var failed = false

    val startingPeriod:Int
        get() = start.existancePeriod

    val endingPeriod:Int
        get() = end.existancePeriod

    val cost: Double
        get()  = updateCost()

    val start: Node
        get() = sequence.first().start

    val end: Node
        get() = sequence.last().end

    companion object {

        fun join (path1: Path, path2: Path?) : Path {

            val path = Path()
            path.sequence.addAll(path1.sequence)
            path.sequence.addAll(path2!!.sequence)

            return path
        }
    }

    fun addEdge(edge: Edge) {

        if (sequence.isEmpty() || edge.start === end) {

            sequence.add(edge)

            if (mCost == java.lang.Double.POSITIVE_INFINITY)
                mCost = 0.0
            mCost += edge.cost
        }
    }

    fun join(path: Path?) {

            if (path?.start === end) {
                sequence.addAll(path.sequence)
                mCost += path.cost
            }
    }

    private fun updateCost(): Double {

        if (mCost == java.lang.Double.POSITIVE_INFINITY && !sequence.isEmpty())
            mCost = sequence.stream().mapToDouble { e -> e.cost }.sum()
        return mCost
    }

    override fun toString(): String {

        val start: Node = sequence.first().start
        val    end: Node = sequence.last().end

        return "[${"%.2f".format(cost)}] ${start.blueprint?.id}:${start.existancePeriod} -> ${end.blueprint?.id}:${end.existancePeriod} [${sequence.size}]"
    }
}
