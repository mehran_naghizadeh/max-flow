package code

enum class NodeLabels (label:String){
    TRIVIAL ("BASICTRIVIALLABEL"),
    VIRTUAL_INPUT ("virtual input"),
    VIRTUAL_OUTPUT ("virtual output")
}
