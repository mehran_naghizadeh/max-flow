package code

enum class FlowCalculationAlgorithm (val v:Boolean)
{

    SINGLE_COMMODITY_FLOW (true),
    MULTI_COMMODITY_FLOW  (false)

}