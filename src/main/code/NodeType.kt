package code

enum class NodeType {

    VIRTUAL,
    ORDINARY,
    HELPER,
    WAITING
}
