package code

data class Commodity (val source: Node, val sink: Node) {

    @JvmField
    var demand:Int = 0

}
