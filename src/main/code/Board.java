package code;

import code.maxFlow.MultiCommodityMaxFlowCalculator;
import code.maxFlow.SingleCommodityMaxFlowCalculator;
import code.util.Util;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import jdk.internal.org.objectweb.asm.tree.MultiANewArrayInsnNode;

import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class Board extends Application {

    private static final int   NUM_STEPS    = 12;
    private static final int   STEP_LENGTH = 1;
    private FXMLLoader        loader;

    private final int RADIUS                            = 15;
    private final int ROW_DISTANCE           = 53;
    private final int COLUMN_DISTANCE   = 63;

    private Pane                                 root;

    private Manager                        manager;
    private Map<Integer, Line>  lines = new HashMap<>();
    private Graph                               baseGraph;    //--- original non-expanded graph
    private Graph                               singleGraph; //--- singleCommodityFlowExpandedGraph
    private Graph                               multiGraph;  //--- multiCommodityFlowExpandedGraph

    private Group nodesLayer;
    private Group edgesLayer;

    private final double PADDING = 2 * RADIUS;
    private boolean originalGraphDrawn = false;

    @FXML
    private TabPane tabPane;

    private AnchorPane stage;
    @FXML protected AnchorPane originalGraphStage;
    @FXML protected AnchorPane singleCommodityFlowGraphStage;
    @FXML protected AnchorPane multiCommodityFlowGraphStage;

    private ListView pathsListView;
    @FXML protected ListView singleCommodityFlowPathsListView;
    @FXML protected ListView multiCommodityFlowPathsListView;

    private ChoiceBox<Integer> startbox;
    @FXML public ChoiceBox<Integer> singleCommodityFlowStartNodeChoicebox;
    @FXML public ChoiceBox<Integer> multiCommodityFlowStartNodeChoicebox;

    private ChoiceBox<Integer> endbox;
    @FXML public ChoiceBox<Integer> singleCommodityFlowEndNodeChoicebox;
    @FXML public ChoiceBox<Integer> multiCommodityFlowEndNodeChoicebox;

    private Label pathCountLabel;
    @FXML private Label singleCommodityFlowPathCountLabel;
    @FXML private Label multiCommodityFlowPathCountLabel;

    private Button flowButton;
    @FXML protected Button singleCommodityFlowButton;
    @FXML protected Button multiCommodityFlowButton;

    private Button searchButton;
    @FXML private Button singleCommodityFlowSearchButton;
    @FXML private Button multiCommodityFlowSearchButton;

    private Button refreshbutton;
    @FXML private Button singleCommodityFlowRefreshButton;
    @FXML private Button multiCommodityFlowRefreshButton;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Conveyor Simulation");

        try
        {
            loader = new FXMLLoader(getClass().getResource("../board.fxml"));
            root = loader.load();

            Scene scene = new Scene(root, 1200, 700, Color.TEAL);
            primaryStage.setScene(scene);
            primaryStage.show();

            manager = new Manager();
            boolean initialized = manager.initialize();

            if (initialized)
            {
                initializeBasicGraph();
                configureUI();

                //calculateSingleMaxFlow(singleGraph);
                //performExpandedSearch(startbox.getValue(), endbox.getValue());
                //draw (multiGraph, BASIC);
                drawOriginalGraph();
            }
/*
            new Timer().scheduleAtFixedRate(
                    new TimerTask() {
                        @Override
                        public void run() {
                            javafx.application.Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    //--simulate();
                                }
                            });
                        }
                    },
                    0,
                    500
            );
*/
        }
        catch (Exception e) {

            Util.error(Board.class, "start", e);

            Map<Class, Integer> map = new HashMap<>();
            map.put (LoadException.class,                -1000);
            map.put (IOException.class,                      -1001);
            map.put (NullPointerException.class,  -1002);
            map.put (IllegalStateException.class, -1003);

            System.exit (map.get(e.getClass()));
        }
    }

    private void configureUI() {

        configureTabPane();
        configureOriginalTab();
        configureSingleFlowTab();
        configureMultiFlowTab();
/*
        configurePathCountLabel();
        configureChoiceboxes();
        configurePathListView();
        configureFlowButton();
        configureSearchButton();
*/
    }

    private void configureTabPane() {

        tabPane = (TabPane) loader.getNamespace().get("tabPane");

        tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {

                System.out.println("Switched to TAB " + newValue.getId());
                switch (newValue.getId()) {

                    case "originalGraphTab":
                        initializeBasicGraph();
                        if (! originalGraphDrawn) drawOriginalGraph();
                        break;

                    case "singleCommodityGraphTab":

                        initializeSingleCommodityGraph();

                        stage                       = singleCommodityFlowGraphStage;
                        startbox                 = singleCommodityFlowStartNodeChoicebox;
                        endbox                    = singleCommodityFlowEndNodeChoicebox;
                        searchButton       = singleCommodityFlowSearchButton;
                        flowButton            = singleCommodityFlowButton;
                        refreshbutton     = singleCommodityFlowRefreshButton;
                        pathsListView     = singleCommodityFlowPathsListView;
                        pathCountLabel = singleCommodityFlowPathCountLabel;

                        configureChoiceboxes    (singleGraph);
                        configureSearchButton  (singleGraph);
                        configureFlowButton      (singleGraph, FlowCalculationAlgorithm.SINGLE_COMMODITY_FLOW);
                        configureRefreshButton(singleGraph);

                        //drawSingleCommodityGraph();
                        break;

                    case "multiCommodityGraphTab":

                        initializeMultiCommodityGraph();

                        stage                       = multiCommodityFlowGraphStage;
                        startbox                = multiCommodityFlowStartNodeChoicebox;
                        endbox                   = multiCommodityFlowEndNodeChoicebox;
                        searchButton      = multiCommodityFlowSearchButton;
                        flowButton           = multiCommodityFlowButton;
                        refreshbutton    = multiCommodityFlowRefreshButton;
                        pathsListView    = multiCommodityFlowPathsListView;
                        pathCountLabel = multiCommodityFlowPathCountLabel;

                        configureChoiceboxes    (multiGraph);
                        configureSearchButton  (multiGraph);
                        configureFlowButton      (multiGraph, FlowCalculationAlgorithm.MULTI_COMMODITY_FLOW );
                        configureRefreshButton(multiGraph);

                        //drawMultiCommodityGraph();
                        break;

                    default: break;
                }
            }
        });
    }

    private void configureOriginalTab() {
        originalGraphStage                                  = (AnchorPane) loader.getNamespace().get("originalGraphStage") ;
    }
    private void configureSingleFlowTab() {

        singleCommodityFlowGraphStage                        = (AnchorPane)                  loader.getNamespace().get("singleCommodityFlowGraphStage") ;
        singleCommodityFlowStartNodeChoicebox    = (ChoiceBox<Integer>) loader.getNamespace().get("singleCommodityFlowStartNodeChoicebox");
        singleCommodityFlowEndNodeChoicebox       = (ChoiceBox<Integer>) loader.getNamespace().get("singleCommodityFlowEndNodeChoicebox");
        singleCommodityFlowButton                                 = (Button)                              loader.getNamespace().get("singleCommodityFlowButton");
        singleCommodityFlowRefreshButton                = (Button)                              loader.getNamespace().get("singleCommodityFlowRefreshButton");
        singleCommodityFlowSearchButton                  = (Button)                              loader.getNamespace().get("singleCommodityFlowSearchButton");
        singleCommodityFlowPathsListView                = (ListView<Path>)            loader.getNamespace().get("singleCommodityFlowPathsListView");
        singleCommodityFlowPathCountLabel             = (Label)                                 loader.getNamespace().get("singleCommodityFlowPathCountLabel");
    }
    private void configureMultiFlowTab() {

        multiCommodityFlowGraphStage                         = (AnchorPane)                  loader.getNamespace().get("multiCommodityFlowGraphStage") ;
        multiCommodityFlowStartNodeChoicebox     = (ChoiceBox<Integer>) loader.getNamespace().get("multiCommodityFlowStartNodeChoicebox");
        multiCommodityFlowEndNodeChoicebox        = (ChoiceBox<Integer>) loader.getNamespace().get("multiCommodityFlowEndNodeChoicebox");
        multiCommodityFlowButton                                  = (Button)                              loader.getNamespace().get("multiCommodityFlowButton");
        multiCommodityFlowRefreshButton                 = (Button)                              loader.getNamespace().get("multiCommodityFlowRefreshButton");
        multiCommodityFlowSearchButton                   = (Button)                              loader.getNamespace().get("multiCommodityFlowSearchButton");
        multiCommodityFlowPathsListView                 = (ListView<Path>)            loader.getNamespace().get("multiCommodityFlowPathsListView");
        multiCommodityFlowPathCountLabel              = (Label)                                 loader.getNamespace().get("multiCommodityFlowPathCountLabel");
    }

    private void configurePathCountLabel() {
        pathCountLabel = singleCommodityFlowPathCountLabel;
    }
    private void configureChoiceboxes(Graph graph) {

        startbox.getItems().addAll(
                graph.getMSources().stream()
                        .map(s -> s.getId())
                        .collect(Collectors.toList())
        );

        endbox.getItems().addAll(
                graph.getMSinks().stream()
                        .map(s -> s.getId())
                        .collect(Collectors.toList())
        );

        startbox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                try {
                    performExpandedSearch (
                            graph,
                            startbox.getItems().get((int)newValue).intValue(),
                               endbox.getValue()
                    );
                } catch (NullPointerException e) {}
            }
        });
        endbox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                try {
                    performExpandedSearch (
                            graph,
                            startbox.getValue(),
                               endbox.getItems().get((int)newValue).intValue()
                    );
                } catch (NullPointerException e) {}
            }
        });

        try {
            startbox.setValue(startbox.getItems().get(startbox.getItems().size() - 1).intValue());
               endbox.setValue(   endbox.getItems().get(   endbox.getItems().size() - 1).intValue());
        } catch (IndexOutOfBoundsException exc) {}
    }
    private void configurePathListView(Graph graph) {

        pathsListView.getItems().clear();

        pathsListView.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                drawExpanded(graph, DrawMode.SEARCH);
                drawPath((Path) pathsListView.getItems().get((int) newValue));
            }
        });

        pathsListView.getItems().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change c) {
                pathCountLabel.setText(pathsListView.getItems().size() + " paths");
            }
        });
    }
    private void configurePathListView(ListView<Path> pathsListView, Label pathCountLabel) {

        pathsListView.getItems().clear();
        /*
        pathsListView.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                draw(singleGraph, DrawMode.SEARCH);
                drawPath(pathsListView.getItems().get((int) newValue));
            }
        });
        */
        pathsListView.getItems().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change c) {
                pathCountLabel.setText(pathsListView.getItems().size() + " paths");
            }
        });
    }
    private void configureFlowButton(Graph graph,  FlowCalculationAlgorithm algorithm) {

        flowButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                calculateFlow(graph, algorithm);
            }
        });
    }
    private void configureRefreshButton(Graph graph) {

        refreshbutton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                drawExpanded(graph, DrawMode.BASIC);
            }
        });
    }
    private void configureSearchButton(Graph graph) {

        searchButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                performExpandedSearch(graph, startbox.getValue(), endbox.getValue());
            }
        });
    }

    private void performExpandedSearch (Graph graph,  int start, int end) {

        //Graph graph = mAlgorithm == FlowCalculationAlgorithm.SINGLE_COMMODITY_FLOW ? singleGraph : multiGraph;
        //draw (graph, DrawMode.SEARCH);
        findExpandedPath (graph, start, end);
    }

    private void drawBasicGraph (Graph graph, DrawMode mode) {

        if (clearStage())
        {
            drawBasicNodes(graph, nodesLayer);
            drawBasicEdges(graph, edgesLayer, mode);
        }
    }
    private void drawExpanded (Graph graph, DrawMode mode) {

        clearStage();

        drawExpandedNodes(graph, nodesLayer);
        drawExpandedEdges(graph, edgesLayer, mode);
    }

    private boolean clearStage() {

        if (stage.getChildren().size() != 2)
        {
            stage.getChildren().clear();

            edgesLayer = new Group();
            stage.getChildren().add(edgesLayer);

            nodesLayer = new Group();
            stage.getChildren().add(nodesLayer);

            return true;
        }
        else
        {
            edgesLayer = (Group) stage.getChildren().get(0);
            nodesLayer = (Group) stage.getChildren().get(1);

            nodesLayer.getChildren().clear();
            edgesLayer.getChildren().clear();

            return false;
        }
    }

    private void drawExpandedNodes  (Graph graph, Group layer) {

        for (Node node : graph.getNodes())
        {
            javafx.scene.shape.Circle rect = new javafx.scene.shape.Circle(
                    plotExpandedX (node),
                    plotExpandedY (node, graph),
                    RADIUS
            );

            rect.setStroke (Color.BLACK);
            rect.setStrokeWidth (0.3);

            if (graph.getMSinks().contains(node))
                rect.setFill (Color.ORANGE);

            else if (graph.getMSources().contains(node))
                rect.setFill (Color.TOMATO);

            else if (graph.getSink() == node)
                rect.setFill (Color.GREENYELLOW);

            else if (graph.getSource() == node)
                rect.setFill (Color.YELLOW);

            else
                rect.setFill (Color.DARKSLATEGRAY);


            Text t = new Text ("" + node.getId());
            t.applyCss();


            if (graph.getMSinks().contains(node) || graph.getMSources().contains(node) || (graph.getSink() == node) || graph.getSource() == node)
                t.setFill(Color.BLACK);

            else
                t.setFill(Color.WHITE);

            t.setTextAlignment(TextAlignment.CENTER);
            t.setX (rect.getCenterX() - t.getLayoutBounds().getWidth() / 2);
            t.setY (rect.getCenterY() + t.getLayoutBounds().getHeight()/4);

            layer.getChildren().add(rect);
            layer.getChildren().add(t);
        }
    }
    private void drawExpandedEdges  (Graph graph, Group layer, DrawMode mode) {

        lines.clear();

        graph.getEdges().stream()
                //.filter(e -> Arrays.asList(new int[]{0, 518}).contains(e.label))
                .forEach(
                        edge ->
                        {
                            Line line = new Line(
                                    plotExpandedX(edge.getStart()),
                                    plotExpandedY(edge.getStart(), graph),
                                    plotExpandedX(edge.getEnd()),
                                    plotExpandedY(edge.getEnd(), graph)
                            );
                            line.setStrokeWidth(3);
                            line.setStroke(Color.CADETBLUE);

                            final int MAGNITUDE = 3;

                            line.setOnMouseEntered(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {

                                    Line line = (Line) event.getTarget();
                                    //line.setStrokeWidth(line.getStrokeWidth() * MAGNITUDE);
                                    line.setStroke(Color.TOMATO);
                                    line.getStrokeDashArray().addAll(6d);

                                    int id =
                                            lines.entrySet().stream()
                                                    .filter(e -> e.getValue() == line)
                                                    .collect(Collectors.toList())
                                                    .get(0)
                                                    .getKey();
                                    Edge e = graph.getEdges().stream()
                                            .filter(ed -> ed.getId() == id)
                                            .collect(Collectors.toList())
                                            .get(0);

                                    //System.out.println("On Edge: " + e);
                                }
                            });
                            line.setOnMouseExited(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    Line line = (Line) event.getTarget();
                                   //line.setStrokeWidth(line.getStrokeWidth() / MAGNITUDE);
                                    line.getStrokeDashArray().clear();
                                    line.setStroke(Color.CADETBLUE);
                                }
                            });

                            switch (mode) {

                                case BASIC:
                                case SEARCH:
                                             layer.getChildren().add(line);
                                            break;

                                case FLOW:
                                            if (0 < edge.getFlow() && edge.getFlow() < Integer.MAX_VALUE && ! edge.isVirtual())
                                                layer.getChildren().add(line);
                                            break;

                                default:
                                            break;
                            }

                            lines.put(edge.getId(), line);
                        }
                );
    }

    private void drawBasicNodes  (Graph graph, Group layer) {

        for (Node node : graph.getNodes())
        {
            javafx.scene.shape.Circle rect = new javafx.scene.shape.Circle(
                    plotBasicX(node),
                    plotBasicY(node),
                    RADIUS
            );

            rect.setStroke (Color.BLACK);
            rect.setStrokeWidth (0.3);

            if (graph.getMSinks().contains(node))
                rect.setFill (Color.ORANGE);

            else if (graph.getMSources().contains(node))
                rect.setFill (Color.TOMATO);

            else if (graph.getSink() == node)
                rect.setFill (Color.GREENYELLOW);

            else if (graph.getSource() == node)
                rect.setFill (Color.YELLOW);

            else
                rect.setFill (Color.DARKSLATEGRAY);

            Text t = new Text ("" + node.getId());
            t.applyCss();

            if (graph.getMSinks().contains(node) || graph.getMSources().contains(node) || (graph.getSink() == node) || graph.getSource() == node)
                t.setFill(Color.BLACK);

            else
                t.setFill(Color.WHITE);

            t.setTextAlignment(TextAlignment.CENTER);
            t.setX (rect.getCenterX() - t.getLayoutBounds().getWidth() / 2);
            t.setY (rect.getCenterY() + t.getLayoutBounds().getHeight()/4);

            layer.getChildren().add(rect);
            layer.getChildren().add(t);
        }
    }
    private void drawBasicEdges  (Graph graph, Group layer, DrawMode mode) {


        lines.clear();

        graph.getEdges().stream()
                //.filter(e -> Arrays.asList(new int[]{0, 518}).contains(e.label))
                .forEach(
                        edge ->
                        {
                            Line line = new Line(
                                    plotBasicX (edge.getStart()),
                                    plotBasicY (edge.getStart()),
                                    plotBasicX (edge.getEnd()),
                                    plotBasicY (edge.getEnd())
                            );
                            line.setStrokeWidth(3);
                            line.setStroke(Color.CADETBLUE);

                            final int MAGNITUDE = 3;

                            line.setOnMouseEntered(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {

                                    Line line = (Line) event.getTarget();
                                    //line.setStrokeWidth(line.getStrokeWidth() * MAGNITUDE);
                                    line.setStroke(Color.TOMATO);
                                    line.getStrokeDashArray().addAll(6d);

                                    int id =
                                            lines.entrySet().stream()
                                                    .filter(e -> e.getValue() == line)
                                                    .collect(Collectors.toList())
                                                    .get(0)
                                                    .getKey();
                                    Edge e = graph.getEdges().stream()
                                            .filter(ed -> ed.getId() == id)
                                            .collect(Collectors.toList())
                                            .get(0);

                                    //System.out.println("On Edge: " + e);
                                }
                            });
                            line.setOnMouseExited(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    Line line = (Line) event.getTarget();
                                   //line.setStrokeWidth(line.getStrokeWidth() / MAGNITUDE);
                                    line.getStrokeDashArray().clear();
                                    line.setStroke(Color.CADETBLUE);
                                }
                            });

                            switch (mode) {

                                case BASIC:
                                case SEARCH:
                                             layer.getChildren().add(line);
                                            break;

                                case FLOW:
                                            if (0 < edge.getFlow() && edge.getFlow() < Integer.MAX_VALUE && ! edge.isVirtual())
                                                layer.getChildren().add(line);
                                            break;

                                default:
                                            break;
                            }

                            lines.put(edge.getId(), line);
                        }
                );
    }

    private void drawOriginalGraph() {
        stage = originalGraphStage;
        drawBasicGraph (baseGraph);
        originalGraphDrawn = true;
    }
    private void drawBasicGraph(Graph graph) {
        drawBasicGraph(graph, DrawMode.BASIC);
    }

    private void drawSingleCommodityGraph() {
        stage = singleCommodityFlowGraphStage;
        drawExpanded(singleGraph, DrawMode.BASIC);
    }
    private void drawMultiCommodityGraph() {
        stage = multiCommodityFlowGraphStage;
        drawExpanded(multiGraph, DrawMode.BASIC);
    }

    private double plotBasicX (Node node) {

        final double w = baseGraph.getMinX();
        final double W = baseGraph.getMaxX();

        final double s = PADDING;
        final double S = stage.getWidth() - PADDING;

        return Util.fitInterval(node.getX(), w, W, s, S);
    }
    private double plotBasicY (Node node)   {

        final double h = baseGraph.getMinY();
        final double H = baseGraph.getMaxY();

        final double s = PADDING;
        final double S = stage.getHeight() - PADDING;

        return Util.fitInterval (node.getY(), h, H, s,  S);
    }

    private double plotExpandedX(Node node) { return PADDING + (1 + node.getExistancePeriod()) * COLUMN_DISTANCE;}
    private double plotExpandedY(Node node, Graph graph) {

        long actualNodes = graph.getNodes().stream().filter(n -> n.getExistancePeriod() == 0).count();

        long y = node.getType() != NodeType.VIRTUAL ?
                ((node.getId()-1) % actualNodes) :
                (actualNodes / 2) ;

        return PADDING + y * ROW_DISTANCE;
    }

    private List<Path> findAllPaths(Graph graph, int start, int end) {
        return graph.findAllPaths(start, end);
    }
    private boolean findShortestPath   (Graph graph, int start, int end) throws NullPointerException{

        Path path = graph.findShortestPath(start, end);
        if (path != null && path.getSequence().get(0).getStart().getExistancePeriod() == 0) addPathToList(path);
        return path != null;
    }
    private void         findExpandedPath (Graph graph, int start, int end) {

        System.out.println(String.format("Searching for a path from %d to %d", start, end));

        pathsListView.getItems().clear();

        List<Node> starts  = graph.getNodes().stream().filter(n -> n.getBlueprint().getId() == start).collect(Collectors.toList());
        List<Node> ends     = graph.getNodes().stream().filter(e -> e.getBlueprint().getId() == end   ).collect(Collectors.toList());

        List<Path> allPaths = new ArrayList<>();

        for (Node s : starts)
            for (Node e : ends)
                allPaths.addAll (findAllPaths (graph, s.getId(), e.getId()));

        if (allPaths.isEmpty())
            System.out.println(String.format("No path found from %d to %d", start, end));
        else {
            addAllPathsToList(allPaths);
            pathsListView.getSelectionModel().select(0);
            drawPath((Path) pathsListView.getItems().get(0));
        }
    }

    private void drawPath(Path path) {

        //System.out.println(String.format("java.Path from %d to %d", path.getSequence().get(0).getStart().getId(), path.getSequence().get(path.getSequence().size() - 1).getEnd().getId()));
        for (Line line : lines.values()) line.setOpacity(.1);

        int[] lineIndexes = path.getSequence().stream().mapToInt(Edge::getId).toArray();
        for (int lineIndex : lineIndexes)
        {
            Line line = lines.get(lineIndex);
            line.setOpacity(1);
            line.setStroke(Color.BLACK);
        }
    }
    private void addPathToList(Path path) {
        pathsListView.getItems().add(path);
    }
    private void addAllPathsToList( List<Path> allPaths ) {

        for (Path path : allPaths)
            addPathToList(path);
    }

    private void calculateFlow (Graph graph, FlowCalculationAlgorithm algorithm) {

        switch (algorithm) {

            case SINGLE_COMMODITY_FLOW:
                calculateSingleMaxFlow(graph);
                break;

            case MULTI_COMMODITY_FLOW:
                calculateMultiMaxFlow(graph);
                break;

            default:
                Util.message (this.getClass(), "calculateFlow", "Invalid algorithm supplied");
                break;
        }
    }
    private void calculateSingleMaxFlow (Graph graph) {

        double maxFlow =  SingleCommodityMaxFlowCalculator.Companion.getMaxFlow(graph);

        System.out.println("Maxflow: " + maxFlow);
        System.out.println(String.format("Flow Distribution from: %s to %s", graph.getSource(), graph.getSink()));
        System.out.println(graph.getEdges());

        drawExpanded (graph, DrawMode.FLOW);
    }
    private void calculateMultiMaxFlow (Graph graph) {

        double maxFlow =  MultiCommodityMaxFlowCalculator.Companion.getMaxFlow(graph);

        System.out.println("Multi Commodity Maximum Flow: " + maxFlow);
        //System.out.println(String.format("Flow Distribution from: %s to %s", graph.getSource(), graph.getSink()));
        //System.out.println(graph.getEdges());

        drawExpanded (graph, DrawMode.FLOW);
    }

    private void initializeBasicGraph() {
        if (baseGraph == null )
            baseGraph = manager.mGraph;
    }
    private void initializeSingleCommodityGraph() {
        if (singleGraph == null ) {
            manager.expandForSingleCommodity(NUM_STEPS, STEP_LENGTH);
            singleGraph = manager.mSingleCommodityFlowExpandedGraph;
        }
    }
    private void initializeMultiCommodityGraph() {
        if (multiGraph == null ) {
            manager.expandForMultiCommodity(NUM_STEPS, STEP_LENGTH);
            multiGraph = manager.mMultiCommodityFlowExpandedGraph;
        }
    }
}
