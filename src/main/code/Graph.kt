package code

import code.util.Util
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.io.*
import java.util.*
import java.util.stream.Stream
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.math.hypot


class Graph {

    private val mNodes: MutableMap<Int, Node> = mutableMapOf()
    val nodes:Set<Node>
        get() = mNodes.values.toSet()

    private val mEdges: MutableMap<String, Edge> = mutableMapOf()
    val edges:Set<Edge>
        get() = mEdges.values.toSet()

    protected val mSinks:       MutableSet<Node> = mutableSetOf()
    protected val mSources: MutableSet<Node> = mutableSetOf()

    val mCommodities: MutableSet<Commodity> = mutableSetOf()

    val adjacencies :MutableMap<Int, LinkedList<Edge>> = mutableMapOf()
    val incidencies   :MutableMap<Int, LinkedList<Edge>> = mutableMapOf()

    var source: Node? = null
    var sink: Node? = null

    val numNodes:Int
        get() = nodes.size

    val numEdges:Int
        get() = edges.size

    val minX: Double
        get() = nodes.minBy { it.x }?.x!!
    val maxX: Double
        get() = nodes.maxBy { it.x }?.x!!

    val minY: Double
        get() = nodes.minBy { it.y }?.y!!
    val maxY: Double
        get() = nodes.maxBy { it.y }?.y!!

    companion object {

        val capacities : MutableSet<Triple<Int, Int, Pair<Double, Double>>> = mutableSetOf()

    }

    constructor() {

        capacities.add(Triple(1, 2, Pair(1.0, 2.0)))
        capacities.add(Triple(1, 3, Pair(1.0, 2.0)))
        capacities.add(Triple(1, 5, Pair(2.0, 2.0)))
        capacities.add(Triple(2, 3, Pair(2.0, 2.0)))
        capacities.add(Triple(2, 4, Pair(1.0, 3.0)))
        capacities.add(Triple(2, 7, Pair(1.0, 3.0)))
        capacities.add(Triple(3, 6, Pair(1.0, 3.0)))
        capacities.add(Triple(3, 7, Pair(2.0, 1.0)))
        capacities.add(Triple(4, 7, Pair(1.0, 2.0)))
        capacities.add(Triple(4, 8, Pair(2.0, 1.0)))
        capacities.add(Triple(5, 8, Pair(1.0, 3.0)))
        capacities.add(Triple(5, 6, Pair(1.0, 1.0)))
        capacities.add(Triple(6, 4, Pair(2.0, 1.0)))
        capacities.add(Triple(6, 9, Pair(1.0, 3.0)))
        capacities.add(Triple(6, 10, Pair(1.0, 3.0)))
        capacities.add(Triple(7, 5, Pair(3.0, 1.0)))
        capacities.add(Triple(7, 10, Pair(1.0, 3.0)))
        capacities.add(Triple(7, 11, Pair(1.0, 3.0)))
        capacities.add(Triple(8, 9, Pair(2.0, 3.0)))
        capacities.add(Triple(8, 11, Pair(1.0, 3.0)))

    }

    @Throws(FileNotFoundException::class, IOException::class)
    constructor(file: File) : this() {

        readFromJSONFile(file)
        makeSinks()
        makeSources()
        makeCommodities()
    }

    @Throws(NullPointerException::class, FileNotFoundException::class, IOException::class)
    private fun readFromJSONFile(file: File) {

        val mapper = jacksonObjectMapper()
        val jsonArray:JsonNode = mapper.readTree(file)
        val neighbourhoods = mutableMapOf<Node, Set<JsonNode>>()

        jsonArray.forEach {

            val node = extractNodeFromJSON(it)
            addNode(node)

            //--- prepare neighbourhood data
            neighbourhoods[node] = it["neighbours"].toSet()
        }

        //--- make neighbourhood according to the prepared data
        neighbourhoods.keys.forEach { node ->

            neighbourhoods[node]?.forEach {

                val neighbourId = it["id"]?.asInt()
                val neighbour:Node? = mNodes[neighbourId]

                val edge = addEdge(node, neighbour)
                edge?.length = it["travel time"].asDouble()
                edge?.capacity = it["capacity"].asDouble()
            }
        }
    }
    @Throws(NullPointerException::class, FileNotFoundException::class, IOException::class)
    private fun readFromSCNFile(file: File) {

        val neighbourhoods = HashMap<Node, IntArray>()
        var interrupted:Boolean = false
        file.forEachLine {
            //--- valid lines start with 'vertext'
            if (!interrupted && it.indexOf("vertex") == 0) {

                val components = it.split(", ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val id = Integer.parseInt(components[0].split(": ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                val node = Node(id)

                try {
                    val xString = components[1].split(": ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                    val yString = components[2].split(": ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                    val zString = components[3].split(": ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]

                    node.x = java.lang.Double.parseDouble(xString)
                    node.y = java.lang.Double.parseDouble(yString)
                    node.z = java.lang.Double.parseDouble(zString)
                }
                catch (exc: NumberFormatException) {
                    Util.error(javaClass, "readFromFile", exc)
                }

                val type = components[4].split(": ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                node.type = when (type) {
                    "Waiting_Node" -> NodeType.WAITING
                    "Helper_Node"    -> NodeType.HELPER
                    else                           -> NodeType.ORDINARY                  //--- This also covers the case of NodeType.Basic_Node
                }

                node.label = components[5].split(": ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                addNode(node)

                val neighbourIDs = components[6]
                        .split(": ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                        .replace("[", "")
                        .replace("]", "")
                        .replace(" ", "")
                        .split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                val neighbours = IntArray(neighbourIDs.size)
                neighbourIDs.indices.forEach { index ->
                    try {
                        neighbours[index] = Integer.parseInt(neighbourIDs[index])
                    } catch (exc: NumberFormatException) {
                        val message = "An end point found at label " + node.id
                        Util.message(javaClass, "readFromFile", message)
                    }
                }
                neighbourhoods[node] = neighbours
            }
            else interrupted = true
        }

        // make neighbourhood data
        neighbourhoods.keys.forEach {
            node -> neighbourhoods[node]?.forEach {
                neighbourId -> addEdge(node, mNodes[neighbourId])
            }
        }
    }

    private fun extractNodeFromJSON (json:JsonNode): Node {

        val node = Node(json["id"].asInt())

        node.x = json["x"].asDouble()
        node.y = json["y"].asDouble()
        node.z = json["z"].asDouble()

        node.type = when (json["type"].toString()) {
            "Waiting_Node" -> NodeType.WAITING
            "Helper_Node"    -> NodeType.HELPER
            else                           -> NodeType.ORDINARY                  //--- This also covers the case of NodeType.Basic_Node
        }

        node.label = json["label"].asText()

        return node
    }

    private fun makeSinks()         = makeEndpoints (EndPointType.SINK)
    private fun makeSources()   = makeEndpoints (EndPointType.SOURCE)

    private fun makeCommodities() {

        for (source in mSources)
            for (sink in mSinks)
                mCommodities.add(Commodity(source, sink))
    }

    private fun makeEndpoints(type: EndPointType) {

        val set = endPointSet (type)
         set.addAll (nodes.filter {
                    //--- a source label starts with crane and a sink label starts with ML
                    it.label.indexOf (type.label) == 0
                })
    }

    private fun nextID(list: Collection<Node>): Int =  list.sortedBy { it.id }.first().id + 1

    fun getSinkById      (id: Int): Node? = getEndpointById(EndPointType.SINK,     id)
    fun getSourceById(id: Int): Node? = getEndpointById(EndPointType.SOURCE, id)

    fun getSinkByLabel      (label: String): Node? = getEndpointByLabel(EndPointType.SINK,      label)
    fun getSourceByLabel(label: String): Node? = getEndpointByLabel(EndPointType.SOURCE, label)

    private fun getEndpointById       (type: EndPointType, id: Int)             : Node? = endPointSet(type).find { it.id == id }
    private fun getEndpointByLabel(type: EndPointType, label: String): Node? = endPointSet(type).find { it.label == label }

    private fun endPointSet(type:EndPointType): MutableSet<Node> = when (type) {
        EndPointType.SINK      -> mSinks
        EndPointType.SOURCE -> mSources
    }

    fun addNode(node: Node) = mNodes.putIfAbsent(node.id, node).let {

            //--- if node is added successfully, set the newly inserted node's ordinal number
            node.ordinalNumber = mNodes.size

            //--- Now we add corresponding new entries to the adjacencies and incidences lists
            adjacencies[node.id] = LinkedList()
            incidencies[node.id]   = LinkedList()
        }
    fun addEdge(start: Node?, end: Node?): Edge? {

        var edge: Edge? =  null

        if (nodes.contains(start) && nodes.contains(end))
        {
            edge = Edge(mEdges.size, start!!, end!!)
            mEdges[edge.label] = edge

            val triplet = capacities.find { it.first == edge.start.blueprint?.id ?: edge.start.id && it.second == edge.end.blueprint?.id ?: edge.end.id}
            triplet?.let {
                edge.length       = triplet.third.first
                edge.capacity  = triplet.third.second
            }

            try {
                adjacencies[start.id ]!!.add(edge)
                incidencies  [end.id    ]!!.add(edge)
            }
            catch (exc: NullPointerException) {
                Util.message(javaClass, "addEdge", "${if(adjacencies[start.id] == null) "adjacency" else "incidency"} list is empty")
                Util.error(javaClass, "addEdge", exc)
            }
        }

        return edge
    }

    protected fun getNodeByOrdinalNumber(ordinalNumber: Int): Node? = mNodes.values.find { n -> n.ordinalNumber == ordinalNumber }

    fun expand (numPeriods: Int, stepLength: Double, addVirtualSourceAndSink: Boolean): Graph? {

        if (sink != null || source != null) return null

        val graph = Graph()

        //--- make nodes
        val periods = ArrayList<Int>()
        for (period in 0 until numPeriods)
            mNodes.values.forEach {
                val n = it.clone(graph.mNodes.size + 1)
                n.existancePeriod = period
                graph.addNode(n)
                periods.add(period)
            }

        //--- make edges
        for (originalStart in mNodes.values)
            originalStart.outNeighbours.forEach {

                originalNeighbour ->

                val path = findEdge(originalStart, originalNeighbour)!!
                val cost  = path!!.cost
                val distanceInPeriods = Math.floor(cost / stepLength).toInt()

                graph.mNodes.values.stream()
                        .filter { n -> n.blueprint == originalStart }
                        .forEach {
                            cloneStart ->
                            graph.mNodes.values.stream()
                                    .filter { n -> n.blueprint == originalNeighbour && n.existancePeriod == cloneStart.existancePeriod + distanceInPeriods }
                                    .forEach { cloneNeighbour -> graph.addEdge(cloneStart, cloneNeighbour) }
                        }
            }

        //--- collect source and sink nodes
        graph.mSources.addAll(graph.mNodes.values.filter { mSources.contains(it.blueprint) })
        graph.mSinks      .addAll(graph.mNodes.values.filter {       mSinks.contains(it.blueprint) })

        //--- make commodities
        for (source in graph.mSources.filter { s -> s.existancePeriod == 0 })
            for (sink in graph.mSinks.filter { s -> s.existancePeriod == 0 })
                graph.mCommodities.add(Commodity(source, sink))

        for (commodity in graph.mCommodities)
            commodity.demand = mCommodities.find { it.source.id == commodity.source.id && it.sink.id == commodity.sink.id }?.demand ?: 0

        if (addVirtualSourceAndSink) {

            //--- make the virtual source and sink
            graph.source = Node(graph.mNodes.size + 1, -1)
            graph.source?.blueprint = Node(-1)
            graph.source?.type = NodeType.VIRTUAL
            graph.source?.label = NodeLabels.VIRTUAL_INPUT.toString()
            graph.addNode(graph.source!!)

            graph.sink = Node(graph.mNodes.size + 1, numPeriods)
            graph.sink?.blueprint = Node(-2)
            graph.sink?.type = NodeType.VIRTUAL
            graph.sink?.label = NodeLabels.VIRTUAL_OUTPUT.toString()
            graph.addNode(graph.sink!!)

            //--- make connections to the virtual source and sink
            graph.mSources.forEach { graph.addEdge(graph.source, it) }
            graph.mSinks.forEach { graph.addEdge(it, graph.sink) }
        }
        else {

            graph.nodes.forEach { start ->

                val end = graph.nodes
                        .filter { it.blueprint == start.blueprint ?: start }
                        .find { it.existancePeriod == start.existancePeriod + 1 }

                //--- add an edge of length 1 between each node and its subsequent clone
                val edge = graph.addEdge (start, end)
                edge?.length = 1.0
                edge?.capacity = if (graph.mSources.contains(edge?.start?.blueprint ?: edge?.start)) 500000.0 else 0.0
            }
        }

        return graph
    }//--- end of expand method

    fun findShortestPath(start: Int, end: Int): Path? = findShortestPath(mNodes[start], mNodes[end])
    fun findShortestPath(home: Node?, goal: Node?): Path? = findPaths(home!!, goal!!)?.sorted(compareBy { it?.cost })?.findFirst()?.get()

    fun findAllPaths(start: Int, end: Int):  List<Path> = findAllPaths(mNodes[start], mNodes[end])
    private fun findAllPaths(home: Node?, goal: Node?):  List<Path> {

        val paths:MutableList<Path> =  mutableListOf()
        findPaths(home!!, goal!!)?.forEach { paths.add(it!!) }
        return paths
    }
    private fun findPaths(home: Node, goal: Node): Stream<Path?>? {

        val paths = ArrayList<Path>()

        for (neighbour in home.outNeighbours)
        {
            val path = Path()

            path.addEdge(findEdge(home, neighbour)!!)

            when
            {
                neighbour == goal -> paths . add (path)
                neighbour.outNeighbours.isEmpty() ->
                {
                    path.failed = true
                    paths.add(path)
                }
                else -> findPaths (neighbour, goal)?.forEach { paths.add(Path.join(path, it)) }
            }
        }

        return paths.stream()
                .filter { ! it.failed }
                .sorted(compareBy { it.cost })
    }

    private fun findEdge(start: Node, end: Node): Edge? = mEdges[Edge.generateEdgeLabel(start, end)]

    fun sourceIds(): List<Int> = mSources.map { it.id }
    fun sinkIds():       List<Int> =      mSinks.map { it.id }

    fun outEdges(node:Node) = edges.filter { it.start == node }
    fun    inEdges(node:Node) = edges.filter { it.end == node }

    fun addBackEdge (edge:Edge):Edge? = addEdge(edge.end, edge.start)
    fun removeEdge (edge:Edge) = when
        {
            edges.contains(edge) ->
                {
                        //--- update adjacency and incidency data for on nodes' level
                        edge.start .outNeighbours.remove(edge.end  )
                        edge.end    .inNeighbours   .remove(edge.start)

                        //--- remove the edge from edges list
                        mEdges.filter { it.value == edge }.forEach {  mEdges.remove(it.key)  }

                        //--- remove the edge from adjacency and incidency data on graph level
                        adjacencies.values.filter { it.contains(edge) }.forEach { it.remove(edge) }
                        incidencies  .values.filter { it.contains(edge) }.forEach { it.remove(edge) }
                }
            else -> {}
        }
    fun removeNode(node: Node) {
        removeEdges(adjacencies[node.id]?.toSet())
        removeEdges(incidencies  [node.id]?.toSet())
        mNodes.filter { it.value == node }.forEach { mNodes.remove(it.key) }
    }

    fun removeEdges(edges: Collection<Edge>?) = edges?.forEach { removeEdge(it) }

    fun containsNode(node:Node): Boolean = nodes.contains(node)
    fun containsEdge(edge:Edge):Boolean   = edges.contains(edge)

    val nodeIds: Set<Int>
        get() = nodes.map { it.id }.toSet()

    fun incident(node:Node) = incidencies[node.id]

    fun edgeAtTime(start: Node, end: Node, time: Int): Edge? = edges
            .find {
                        (it.start == start || it.start.blueprint == start)
                        &&
                        (it.end == end || it.end.blueprint == end)
                        &&
                        it.existancePeriod == time
            }
    fun edgeAtTime(edge:Edge, time: Int): Edge? = edgeAtTime(edge.start, edge.end, time)
}
