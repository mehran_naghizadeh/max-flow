package code.maxFlow

import code.Edge
import code.Graph
import code.Node
import code.util.Util
import java.util.Comparator
import java.util.HashMap
import java.util.LinkedList
import java.util.HashSet

class SingleCommodityMaxFlowCalculator {

    companion object {

            class DistanceLabels {

                private var labels = hashMapOf<Node, Int>()
                private val nodes: IntArray

                constructor(n: Int) {
                    nodes = IntArray(n + 1)
                }

                fun getNodeLabel(n: Node) = labels[n]
                fun setNodeLabel(n: Node?, label: Int): Boolean {

                    var existsUnassignedLabel = false
                    val oldLabel: Int? = labels[n]
                    if (oldLabel != null) {
                        nodes[oldLabel]--
                        if (nodes[oldLabel] === 0) existsUnassignedLabel = true
                    }

                    labels[n!!] = label
                    nodes[label]++
                    return existsUnassignedLabel
                }
            }

            fun getMaxFlow(g: Graph): Double {

                if (g.numNodes === 0)
                    return 0.0

                val labels = calcDistanceLabels(g)
                var f = 0.0 // max flow
                val n = g.numNodes
                val backEdges = addBackEdges(g)
                val path = LinkedList<Edge>()
                var i = g.source

                while (i != null && labels.getNodeLabel(g.source!!)!! < n)
                {
                    val e = getAdmissibleEdge(g, i, labels)
                    if (e != null)
                    {
                            i = advance(e, path)
                            if (i == g.sink)
                            {
                                f += augment(g, path)
                                i = g.source
                                path.clear()
                            }
                    }
                    else
                    {
                        //Util.message(javaClass, "getMaxFlow", "no admissible edge for " + i)
                        i = retreat(g, labels, i, path)
                    }
                }

                removeBackEdges(g, backEdges)

                return f
            }

            fun calcDistanceLabels(g: Graph): DistanceLabels {

                val n = g.numNodes
                val labels = DistanceLabels(n)
                val visited = HashSet<Node>()

                g.nodes.forEach { labels.setNodeLabel(it, n) }

                try {
                    labels.setNodeLabel(g.sink!!, 0)
                } catch (exc: NullPointerException) {
                    println(exc.localizedMessage)
                }

                val queue = LinkedList<Node>()
                queue.addLast(g.sink)

                while (!queue.isEmpty()) {

                    val j = queue.removeFirst()

                    g.inEdges(j).forEach {
                        val i = it.start
                        if (!visited.contains(i)) {
                            labels.setNodeLabel(i, labels.getNodeLabel(j)!! + 1)
                            visited.add(i)
                            queue.addLast(i)
                        }
                    }
                    visited.add(j)
                }

                return labels
            }
            fun addBackEdges(g: Graph): List<Edge?> {

                val backEdges = LinkedList<Edge?>()

                g.edges.forEach {
                    val backEdge = g.addBackEdge(it)
                    backEdge?.capacity = 0.0
                    backEdge?.flow          = 0.0
                    backEdges.add(backEdge)
                }
                return backEdges
            }
            fun getAdmissibleEdge(g: Graph, i: Node, d: DistanceLabels): Edge? = g.outEdges(i)?.find { it.residualCapacity > 0.0 && d.getNodeLabel(i) === 1 + d.getNodeLabel(it.end)!! }

            fun advance(e: Edge, path: LinkedList<Edge>): Node {
                path.addLast(e)
                return e.end
            }
            fun augment(g: Graph, path: LinkedList<Edge>): Double {

                val delta = path.map { it.residualCapacity }.min()

                path.forEach {e ->

                    e.flow += delta ?: 0.0

                    var revEdge: Edge? = g.inEdges(e.start).find { it.start == e.end }
                    revEdge?.capacity  = revEdge!!.capacity + (delta ?: 0.0)
                    if (revEdge.flow > 0)
                        revEdge.flow -= delta ?: 0.0
                }

                return delta ?: Double.MAX_VALUE
            }
            fun retreat(g: Graph, labels: DistanceLabels, i: Node, path: LinkedList<Edge>): Node? {

                //--- dj = labels.getNodeLabel(it.end) as Int
                var djMin = g.outEdges(i).filter { it.residualCapacity > 0 }.map { labels.getNodeLabel(it.end) as Int }.min()
                var dMin = Math.min( g.numNodes - 1,  djMin ?: Int.MAX_VALUE)

                val flag = labels.setNodeLabel (i, 1 + dMin)

                val predecessor = when(flag) {
                    true -> null
                    false -> if (i === g.source) i else path.removeLast().start
                }

                return predecessor
            }
            fun removeBackEdges(g: Graph, backEdges: List<Edge?>) = backEdges.forEach { g.removeEdge(it!!) }
    }
}