package code.maxFlow

import code.Commodity
import code.Edge
import code.Graph
import code.Node
import gurobi.*
import java.util.*

class MultiCommodityMaxFlowCalculator {

    companion object {

        fun getMaxFlow (graph: Graph): Double = MultiCommodityMaxFlowCalculator().getMaxFlow(graph)

    }

    private fun getMaxFlow (graph: Graph): Double {

        this.graph = graph

        val t = graph.edges.filter { it.end.blueprint == it.start.blueprint }

        makeTheModelObject(4, 4096)
        setupTheModel()
        optimize()

        //println("Objective: " + model.get (GRB.DoubleAttr.ObjVal))

        return 0.0//model.get(GRB.DoubleAttr.ObjVal)
    }

    private val env      : GRBEnv        = GRBEnv()
    private val model: GRBModel   = GRBModel(env)
    private val x           : MutableMap< Pair<Commodity, Edge>, GRBVar > = mutableMapOf()
    private val y           : MutableMap< Pair<Commodity, Edge>, GRBVar > = mutableMapOf()
    private val v           : GRBVar = model.addVar(0.0, Double.POSITIVE_INFINITY, 1.0, GRB.CONTINUOUS, "v")
    private var graph:Graph = Graph()

    private val T:Int
        get() = graph.nodes.map { it.existancePeriod }.max() ?: 0

    private val K
        get() = graph.mCommodities

    private val A
        get() = graph.edges.filter { it.existancePeriod == 0 }

    private val N
        get() = graph.nodes.filter { it != graph.source && it != graph.sink && !S.contains(it) && !D.contains(it) }

    private val S
        get() = graph.nodes.filter { graph.getSourceById(it.blueprint?.id ?: it.id) != null }

    private val D
        get() = graph.nodes.filter { graph.getSinkById      (it.blueprint?.id ?: it.id) != null }

    private fun makeTheModelObject (numThreads:Int, maxMemInMegaBytes:Int) {

        env.set (GRB.IntParam.Threads, numThreads)
        env.set (GRB.IntParam.LogToConsole, 1)
        env.set (GRB.DoubleParam.NodefileStart, maxMemInMegaBytes / 16384.0)
    }
    private fun setupTheModel() {

        setupVariables()
        model.update()
        setupConstraints()
    }

    private fun setupVariables() {
        setupVariableX()
        setupVariableY()

        val b = y.filter { it.key.second.end.blueprint == it.key.second.start.blueprint ?: it.key.second.start }
    }
    private fun setupVariableX() {

        K.forEach { k ->
            for (t:Int in 0..T)
                graph.edges
                        .filter { it.existancePeriod == t}
                        .forEach {
                            x[Pair(k, it)] = model.addVar(0.0, Double.POSITIVE_INFINITY, 0.0, GRB.CONTINUOUS, "x[${getNameString(k, it, t)}]")
                        }
        }
    }
    private fun setupVariableY() {

        K.forEach { k ->
            for (t:Int in 0..T)
                graph.edges
                        .filter { it.existancePeriod == t }
                        .forEach {
                        y[Pair(k, it)] = model.addVar(0.0,  1.0, 0.0, GRB.BINARY, "y[{${graph.mCommodities.indexOf(k)}},${it.start.id},${it.end.id},$t]")
                }
        }
    }

    private fun getX                        (k: Commodity, e: Edge?): GRBVar?   = x [x.keys.find { it == Pair(k, e) }]
    private fun getY                        (k: Commodity, e: Edge?): GRBVar?   = y [y.keys.find { it == Pair(k, e ) }]

    private fun getNameString (k: Commodity, n1: Node, n2:Node, n3:Node, t: Int): String = "${getNameString(k)},${n1.id},${n2.id},${n3.id},$t"
    private fun getNameString (k: Commodity, t: Int): String                          = "${getNameString(k)},$t"
    private fun getNameString (k: Commodity, n: Node, t: Int): String        = "${getNameString(k)},${n.id},$t"
    private fun getNameString (k: Commodity, e: Edge, t: Int): String        = "${getNameString(k)},${e.start.id},${e.end.id},$t"
    private fun getNameString (e: Edge, t: Int): String                                        = "${e.start.id},${e.end.id},$t"
    private fun getNameString (k: Commodity): String                                       = "${graph.mCommodities.indexOf(k)}"

    private fun incidencies         (node: Node): Set<Node>? = graph.incidencies  [node.id]?.map { it.start.blueprint ?: it.start }?.toSet()
    private fun adjacencies       (node: Node): Set<Node>? = graph.adjacencies[node.id]?.map { it.end.blueprint!! }?.toSet()

    private fun setupConstraints() {

        setupConstraint1()
        setupConstraint2()
        setupConstraint3()
        setupConstraint4()
        setupConstraint5()
        setupConstraint6()
        setupConstraint7()
        setupConstraint8()
        setupConstraint9()
        setupConstraint10()
        setupConstraint11()
        setupConstraint12()
        setupConstraint13()
        setupConstraint14()
        setupConstraint15()
        setupConstraint16()
        setupConstraint17()
    }

    private fun setupConstraint1() {

    }
    private fun setupConstraint2() {

        K.forEach { k ->
            for (t: Int in 0..T)
                incidencies(k.sink)?.forEach { n ->

                    val expr = GRBLinExpr()

                    val e1 = ed(n, k.sink, t)!!
                    val t1 = getY(k, e1)
                    expr.addTerm( +(t + e1.length), t1)

                    val e2 = ed(n, k.sink, t+1)!!
                    val t2 = getY(k, e2)
                    expr.addTerm( -(t + e2.length), t2)

                    try {
                        model.addConstr (expr, GRB.LESS_EQUAL, v, "constraint_2[${getNameString(k, n, t)}]")
                    }
                    catch (exc: NullPointerException) {}
                }
        }
    }
    private fun setupConstraint3() {

        K.forEach { k ->

            val expr = GRBLinExpr()
            adjacencies(k.source)?.forEach { expr.addTerm (1.0, getX(k, ed(k.source, it ,  0))) }
            model.addConstr (expr, GRB.EQUAL, k.demand.toDouble(), "constraint_3[${getNameString(k)}]")

        }
    }
    private fun setupConstraint4() {

        K.forEach { k ->

            val expr = GRBLinExpr()
            incidencies(k.sink)?.forEach {
                for (t: Int in 0.rangeTo(T - distance(it, k.sink)!!))
                    expr.addTerm(1.0, getX(k, ed(it, k.sink, t)))
            }

            model.addConstr (expr, GRB.EQUAL, k.demand.toDouble(), "constraint_4[${getNameString(k)}]")
        }
    }
    private fun setupConstraint5() {

        K.forEach { k ->

            val expr = GRBLinExpr()
            adjacencies(k.source)
                    ?.filter        { k.source != it.blueprint ?: it}
                    ?.forEach   { expr.addTerm(1.0, getY(k, ed(k.source, it, 0))) }

            model.addConstr (expr, GRB.LESS_EQUAL, 1.0, "constraint_5[${getNameString(k)}]")
        }
    }
    private fun setupConstraint6() {

        K.forEach{ k ->
            for (t: Int in 0 until T-1)
            {
                val expr = GRBLinExpr()

                val e1 = ed(k.source, k.source, t+0)
                val t1 = getY(k, e1)
                expr.addTerm(+1.0, t1)

                val e2 = ed(k.source, k.source, t+1)
                val t2 = getY(k, e2)
                expr.addTerm(-1.0, t2)

                try {
                    model.addConstr(expr, GRB.LESS_EQUAL, 0.0, "constraint_6[${getNameString(k, t)}]")
                } catch (exc: NullPointerException) {}
            }
        }
    }
    private fun setupConstraint7() {

        for (t: Int in 0 until T)
            K.forEach { k ->

                //--- make expression object
                val expr = GRBLinExpr()

                //--- fill the expression object
                adjacencies(k.source)
                        ?.filter { k.source != it.blueprint ?: it}
                        ?.forEach { j ->
                                val edge = ed(k.source, j, t+1)
                                edge?.let { expr.addTerm(1.0, getY(k, it))  }
                        }

                expr.addTerm(-1.0, getY(k, ed(k.source, k.source, t)))

                //--- make a constraint with the expression
                model.addConstr(expr, GRB.LESS_EQUAL, 0.0, "constraint_7[${getNameString(k, t)}]")
            }
    }
    private fun setupConstraint8() {

        K.forEach { k ->
            N.forEach { i ->
                for (t:Int in 0..T) {

                    //--- make expression object
                    val expr = GRBLinExpr()

                    //--- fill the expression object
                    adjacencies(i)?.forEach {
                        val edge = ed(i, it, t)
                        edge?.let {expr.addTerm(+1.0, getY(k, it))  }
                    }

                    //--- make a constraint with the expression
                    model.addConstr(expr, GRB.LESS_EQUAL, 1.0, "constraint_8[${getNameString(k, i, t)}]")
                }
            }
        }
    }
    private fun setupConstraint9() {

        K.forEach { k ->
            N.forEach { i ->
                for (t:Int in 0..T)
                {
                    val edge =  ed(i, i, t)
                    edge?.let {
                        val expr = GRBLinExpr()
                        expr.addTerm(1.0, getY(k, it))
                        model.addConstr(expr, GRB.EQUAL, 0.0, "constraint_9[${getNameString(k,i,t)}]")
                    }
                }
            }
        }
    }
    private fun setupConstraint10() {

        K.forEach { k ->
            adjacencies(k.source)
                    ?.filter { k.source != it.blueprint ?: it }
                    ?.forEach { j ->
                        for (t:Int in 0 until T) {

                            val expr = GRBLinExpr()

                            expr.addTerm(-1.0, getY(k, ed(k.source, j, t+1)))
                            expr.addTerm(+1.0, getY(k, ed(k.source, k.source, t)))
                            expr.addTerm(+1.0, getY(k, ed(k.source, j, t)))

                            try {
                                model.addConstr(expr, GRB.LESS_EQUAL, +1.0, "constraint_10[${getNameString(k, j, t)}]")
                            } catch (exc: NullPointerException) {

                            }
                        }
                     }
        }
    }
    private fun setupConstraint11() {

        K.forEach { k ->
            N.forEach { i ->
                adjacencies(i)?.forEach { j ->
                    incidencies(i)?.forEach { m ->

                        for (t:Int in 0 until T)
                            if (t - distance(m, i)!! >= 0)
                            {
                                //--- make an expression object
                                val expr = GRBLinExpr()

                                //--- fill the expression object
                                expr.addTerm(-1.0, getY(k, ed(i, j, t+1)))
                                expr.addTerm(+1.0, getY(k, ed(i, j, t)))
                                expr.addTerm(+1.0, getY(k, ed(m, i, distance(m, i)!!)))
                                expr.addTerm(+1.0, getY(k, ed(m, i, t + 1 - distance(m, i)!!)))

                                //--- make a constraint with the expression
                                try {
                                    model.addConstr(expr, GRB.LESS_EQUAL, 2.0, "constraint_11[${getNameString(k, i, j, m, t)}]")
                                } catch (exc: NullPointerException) {}
                            }
                    }
                }
            }
        }
    }
    private fun setupConstraint12() {

        A.filter { it.end.blueprint != it.start}.forEach { e ->

            for (t:Int in 0..T) {

                val edge = ed(e, t)
                edge?.let {

                    //--- make expression object
                    val expr = GRBLinExpr()

                    //--- fill the expression object
                    K.forEach { k -> expr.addTerm(1.0, getY (k, it)) }

                    //--- make a constraint with the expression
                    model.addConstr(expr, GRB.LESS_EQUAL, 1.0, "constrainr_12[${getNameString(e, t)}]")
                }
            }
        }
    }
    private fun setupConstraint13() {

        A.forEach { e ->
            for (t:Int in 0..T) {

                val edge = ed(e, t)
                edge?.let {

                    //--- make expression object
                    val expr = GRBLinExpr()

                    //--- fill the expression object
                    K.forEach { k -> expr.addTerm(1.0, getX (k, it)) }

                    //--- make a constraint with the expression
                    model.addConstr(expr, GRB.LESS_EQUAL, e.capacity, "constrainr_13[${getNameString(e, t)}]")
                }
            }
        }
    }
    private fun setupConstraint14() {

        K.forEach { k ->
            N.forEach { i ->
                for (t:Int in 0..T)
                    try {
                        //--- make expression object
                        val expr = GRBLinExpr()

                        //--- fill the expression object
                        incidencies(i)  ?.filter { t - distance(it, i)!! >= 0 }?.forEach { j -> expr.addTerm(+1.0, getX(k, ed( j, i, t - distance(j,i)!!))  ) }
                        adjacencies(i)?.filter { t - distance(it, i)!! >= 0 }?.forEach { j -> expr.addTerm(-1.0, getX(k, ed(i, j, t))                                                   ) }

                        //--- make a constraint with the expression
                        model.addConstr(expr, GRB.EQUAL, 0.0, "constrainr_14[${getNameString(k, i, t)}]")
                    }
                    catch (exc:NullPointerException) {}
            }
        }
    }
    private fun setupConstraint15() {

        K.forEach { k ->
            N.forEach { i ->
                for (t:Int in 0..T)
                    try {

                        //--- make expression object
                        val expr = GRBLinExpr()

                        //--- fill the expression object
                        incidencies(i)  ?.filter { t - distance(it, i)!! >= 0 }?.forEach { j -> expr.addTerm(+1.0, getY(k, ed(j, i, t - distance(j, i)!!))  ) }
                        adjacencies(i)?.filter { t - distance(it, i)!! >= 0 }?.forEach { j -> expr.addTerm(-1.0, getY(k, ed(i, j, t - 0                             ))   ) }

                        //--- make a constraint with the expression
                        model.addConstr(expr, GRB.EQUAL, 0.0, "constraint_15[${getNameString(k, i, t)}]")
                    }
                    catch (exc: NullPointerException) {}
            }
        }
    }
    private fun setupConstraint16() {

        K.forEach { k ->
            for (t:Int in 1..T)
                try {
                    //--- make expression object
                    val expr = GRBLinExpr()

                    //--- fill the expression object
                    incidencies(k.source)  ?.forEach { j -> expr.addTerm(+1.0, getX(k, ed(j, k.source,t - distance(j, k.source)!! )) ) }
                    adjacencies(k.source)?.forEach { j -> expr.addTerm(-1.0, getX(k, ed(k.source, j, t - 0                                              )) ) }

                    //--- make a constraint with the expression
                    model.addConstr(expr, GRB.EQUAL, 0.0, "constraint_16[${getNameString(k, t)}]")
                }
                catch (exc: NullPointerException) {}
        }
    }
    private fun setupConstraint17() {

        K.forEach { k ->
            A.forEach { e ->
                for (t: Int in 1..T)
                {
                    val edge = ed(e, t)
                    edge?.let {
                        //--- make expression object
                        val expr = GRBLinExpr()

                        //--- fill the expression object
                        expr.addTerm(+1.0,   getX(k, it))
                        expr.addTerm(-e.capacity, getY(k, it))

                        //--- make a constraint with the expression
                        model.addConstr(expr, GRB.LESS_EQUAL, 0.0, "constraint_17[${getNameString(k, e, t)}]")
                    }
                }
            }
        }
    }

    private fun optimize() = model.optimize()
    private fun ed (edge: Edge, time: Int): Edge? = ed(edge.start, edge.end, time)
    private fun ed (start:Node, end:Node,  time: Int): Edge? = graph.edgeAtTime(start, end, time)
    private fun distance(start: Node, end: Node): Int? = graph.edges.find { it.start == start.blueprint ?: start && it.end.blueprint == end.blueprint ?: end }?.length?.toInt()
}