package code

import java.util.TreeMap

data class Tot (val commodity: Commodity?) {

    val source: Node?
        get() = commodity?.source

    val sink:Node?
        get() = commodity?.sink

    var startTime:Int = 0
    var endTime:Int = 100

    var history: TreeMap<Int, Node>? = null

    private val mPosition: Node? = null
    private val mProperies = TreeMap<String, String>()

    val crane: String
        @Throws(NullPointerException::class)
        get() {
            try {
                val crane = mProperies["crane"]
                return "crane" + if (crane == "N/A") "00" else crane
            } catch (exc: NullPointerException) {
                println(exc.localizedMessage)
                throw exc
            }
        }

    fun setProperty(label: String, value: String) {
        mProperies[label] = value
    }

    fun getProperty(label: String): String? =  mProperies[label]

    companion object {
        val SIZE = 1

        fun parseLine (line:String) {

        }
    }
}
