package code

data class Edge(val id: Int, val start: Node, val end: Node) {

    val label: String = generateEdgeLabel(start, end)

    private var fakeLength:Double = -1.0
    var length: Double
        get() = if (isVirtual) 0.0 else if (fakeLength == -1.0) geoLength() else fakeLength
        set(value) { fakeLength = value }

    private var flowField:Double = 0.0

    var flow: Double
        get() = flowField
        set(value) =
            if (value > capacity) throw IllegalArgumentException("A flow amount ($value) greater than the local capacity ($capacity) is supplied to edge $this")
            else this.flowField = value

    private var capacityField:Double = -1.0
    var capacity: Double
        get() = when
        {
            isVirtual                           -> Double.POSITIVE_INFINITY
            capacityField == -1.0-> Math.floor (length  / Tot.SIZE)
            else                                    -> capacityField
        }
        set(value) {
            capacityField = value
        }

    val residualCapacity
        get() = capacity - flow

    val isVirtual: Boolean
        get() = start.type == NodeType.VIRTUAL || end.type == NodeType.VIRTUAL

    val cost: Double
        get() = length!!

    val existancePeriod:Int
        get() = start.existancePeriod

    init {
        start.outNeighbours.add(end)
        end.inNeighbours.add(start)
    }

    private fun geoLength(): Double = Math.sqrt(Math.pow(end.x - start.x, 2.0) + Math.pow(end.y - start.y, 2.0) + Math.pow(end.z - start.z, 2.0))
    //override fun toString(): String = String.format("edge[%d(%s)]", id, label)
    //override fun toString(): String = "(${start.id}, ${end.id}) [$flow / $capacity]"
    override fun toString(): String {

        //if (isVirtual) return ""

        val sId = start.blueprint?.id ?: start.id
        val eId =    end.blueprint?.id ?: end.id
        val cap = if (capacity == Double.MAX_VALUE) "INF" else capacity.toString()

        val len = if (length == null) "NUL" else length

        //return String.format("(%2d:%-2d->%2d:%-2d| %s)[%.0f/%s]", sId, start.existancePeriod, eId, end.existancePeriod, len.toString(), flow, cap)
        return String.format("(%2d:%-2d->%2d:%-2d)[%.0f/%s]", sId, start.existancePeriod, eId, end.existancePeriod,  flow, cap)
    }
    override fun equals(other: Any?): Boolean = when
        {
            other as? Edge == null -> false

            else -> other .start == this.start && other.end == this.end
        }

    companion object {
        fun generateEdgeLabel(start: Node, end: Node): String = start.id.toString() + "-" + end.id
    }
}
