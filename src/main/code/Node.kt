package code

import java.util.ArrayList

data class Node (val id: Int){

    var x: Double
    var y: Double
    var z: Double
    var type: NodeType = NodeType.ORDINARY
    var outNeighbours: MutableList<Node>
    var inNeighbours: MutableList<Node>
    var label: String
    var ordinalNumber: Int
    var existancePeriod: Int
    var blueprint: Node? = null
    var tag:String = ""
    var capacity:Double = 0.0

    init {
        type = NodeType.ORDINARY
        outNeighbours = ArrayList()
        inNeighbours = ArrayList()
        label = DEFAULT_LABEL
        ordinalNumber = 0
        z = 0.0
        y = z
        x = y
        existancePeriod = 0
        blueprint = null
    }

    constructor (id:Int, tag:String) : this(id) {
        this.tag = tag
    }

    constructor (id: Int, tag:String, existancePeriod: Int) : this(id, tag) {
        this.existancePeriod = existancePeriod
    }
    constructor (id:Int, existancePeriod:Int): this(id) {
        this.existancePeriod = existancePeriod
    }

    fun addDestination(node: Node) {

        if (outNeighbours.stream().filter { n -> n.id == node.id }.count() == 0L) {
            outNeighbours.add(node)
        } else {
            //throw new Exception("Duplicate node label is not acceptable");
        }
    }

    fun addDeparture(node: Node) {

        if (inNeighbours.stream().filter { n -> n.id == node.id }.count() == 0L) {
            inNeighbours.add(node)
        } else {
            //throw new Exception("Duplicate node label is not acceptable");
        }
    }

    fun clone(newId: Int): Node {

        val node = Node(newId, tag)

        node.x = x
        node.y = y
        node.z = z

        node.type = type
        node.label = label

        node.blueprint = this

        return node
    }

    override fun equals(other: Any?) = when {
        other == null || other !is Node -> false
        else -> other.id == this.id
    }
    override fun hashCode(): Int = id
    override fun toString(): String = String.format("node[%d]", id)

    companion object {
        val DEFAULT_LABEL = "BASICTRIVIALLABEL"
    }
}
