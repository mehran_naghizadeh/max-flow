package code;

import code.util.Util;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Manager {

    public Graph        mGraph;
    public Graph        mSingleCommodityFlowExpandedGraph;
    public Graph        mMultiCommodityFlowExpandedGraph;
    public final Set<Tot>  mTots = new HashSet<>();

    //private final File mGraphFile   = new File("/Users/mohammadreza/Desktop/work/Farzaneh/TVH/TVHleft.scn");
    private final File mGraphFile    = new File("data/graph.json"    );
    private final File requestsFile = new File("data/requests.csv");

    public boolean initialize() {

        if (! makeGraph()     )    return false;
        if (! readRequests())    return false;

        makeGurobiModel();

        return true;
    }

    private boolean makeGraph() {

        try
        {
            mGraph = new Graph(mGraphFile);
            return true;
        }
        catch (FileNotFoundException exc)
        {
            Util.error  (getClass(), "Manager", exc);
            Util.message(getClass(), "Manager", "Program aborted due to unavailabality of graph's file " + mGraphFile.getPath());
            return false;
        }
        catch (IOException exc)
        {
            Util.error(getClass(), "Manager", exc);
            Util.message(getClass(), "Manager", "Program aborted due to unavailabality of graph's file");
            return false;
        }
    }
    private boolean readRequests() {
        try
        {
            readRequestFile();
            return true;
        }
        catch (FileNotFoundException exc)
        {
            Util.error(getClass(), "Manager", exc);
            Util.message(getClass(), "Manager", "Program aborted due to unavailabality of requests' file");
            return false;
        }
        catch (IOException exc)
        {
            Util.error(getClass(), "Manager", exc);
            Util.message(getClass(), "Manager", "Program aborted due to unavailabality of requests' file");
            return false;
        }
    }

    private void readRequestFile() throws IOException{

        String headLine;

        List<Tot> list = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(requestsFile));

        headLine = br.readLine();
        String[] headers = headLine.split(",");

        TreeMap<String, String> properties = new TreeMap<>();
        String line;
        int lineNumber = 0;
        while ((line = br.readLine()) != null) {

            lineNumber ++;

            List<String> values = Arrays.asList(line.split(","));
            int index = 0;
            for (String value : values) {
                String key = headers[index++];
                properties.put(key, value);
            }

            try {

                Commodity commodity = mGraph.getMCommodities().stream()
                    .filter(c ->
                            c.getSource().getId()  == Integer.parseInt (properties.get("source"))
                            &&
                            c.getSink().getId()        == Integer.parseInt (properties.get("sink"))
                    )
                    .collect(Collectors.toList())
                    .get(0);

                Tot tot = new Tot(commodity);

                tot.setStartTime   (Integer.parseInt (properties.get ("start_time"  )));
                tot.setEndTime      (Integer.parseInt (properties.get ("end_time"     )));

                commodity.demand ++;

                mTots.add(tot);
            }
            catch (IndexOutOfBoundsException exc) {
                Util.message (
                        this.getClass(),
                        "readRequestFile",
                        String.format("no commodity (%s, %s)", properties.get("sink"), properties.get("source"))
                );
            }
        }
    }

    private void makeGurobiModel() {

    }

    public void expandForSingleCommodity (int steps, double stepLength) {
        mSingleCommodityFlowExpandedGraph = expand(steps, stepLength, FlowCalculationAlgorithm.SINGLE_COMMODITY_FLOW);
    }
    public void expandForMultiCommodity (int steps, double stepLength) {
        mMultiCommodityFlowExpandedGraph = expand(steps, stepLength, FlowCalculationAlgorithm.MULTI_COMMODITY_FLOW);
    }

    private Graph expand (int steps, double stepLength, FlowCalculationAlgorithm algorithm ) {

         return mGraph.expand(
                steps,
                stepLength,
                algorithm == FlowCalculationAlgorithm.SINGLE_COMMODITY_FLOW
        );
    }
}
