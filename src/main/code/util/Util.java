package code.util;

import java.util.Arrays;

public class Util {

    private static String[] blackList = {};//"readFromFile"};

    public static void error(Class c, String methodName, Exception e) {

        if (!Arrays.asList(blackList).contains(methodName))
            System.out.println(String.format("%7s -- %8s::%-17s -> [%s : %s]", "error", c.getName(), methodName, e.getClass().getName(), e.getLocalizedMessage()));
    }

    public static void message(Class c, String methodName, String message) {

        if (!Arrays.asList(blackList).contains(methodName))
            System.out.println(String.format("%7s -- %8s::%-17s -> %s", "message", c.getName(), methodName, message));
    }

    public static double fitInterval (double value, double min, double max, double MIN, double MAX) {
        final double x = max == min ? max  : MIN + ((MAX-MIN)/(max-min)) * (value-min);
        //---System.out.println(String.format("x: %-5.0f -> %-5.0f [%-5.0f|%-5.0f|%-5.0f|%-5.0f]", value, x, min, max, MIN, MAX));
        return x;
    }

}
